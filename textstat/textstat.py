import argparse
import sys
import typing as t
from pathlib import Path
from pprint import pprint


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('file_path')

    args = parser.parse_args()

    stat = gather_word_stat(Path(args.file_path))
    pprint(stat)
    unique_words = find_unique_words(stat)
    pprint(unique_words)


def gather_word_stat(file_path: Path) -> t.Dict[str, int]:
    stat = {}
    with open(str(file_path)) as input_file:
        for line in input_file:
            # Такой сплит строк требует тщательной подготовки входного текста,
            # т.к. не учитывает пунктуации.
            # Может быть, стоит дать на вход текстовый файл не с прозой, а со
            # словами и сказать, что это словарь, составитель которого был
            # невнимателен и допустил кучу ошибок, внеся множество повторов.
            # А боевой входной файл - просто куча слов, где уникально только
            # одно, оно и будет ключом к решению задачи.
            words = line.split()
            for word in words:
                # Ветвления может не быть, если использовать defaultdict
                if not word in stat:
                    stat[word] = 1
                else:
                    stat[word] += 1
    return stat


def find_unique_words(stat: t.Dict[str, int]) -> t.List[str]:
    return [word for word, count in stat.items() if count == 1]


if __name__ == '__main__':
    sys.exit(main())
