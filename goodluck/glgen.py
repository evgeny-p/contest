import argparse
import random
import sys
import textwrap
import typing as t
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('input', help='Goodluck source')
    parser.add_argument('output', help='Obfuscated source')

    args = parser.parse_args()

    content = read_text(args.input)
    obfuscated = ''.join(''.join([op] + generate_stub()) for op in content)
    Path(args.output).write_text(textwrap.fill(obfuscated))


def read_text(file_path: str) -> str:
    return ''.join(line.strip() for line in open(file_path))


def generate_stub() -> t.List[str]:
    stub_length = random.randint(0, 20)
    pos = 0
    stub = []
    for _ in range(stub_length):
        ops = ['+', '-', '>']
        if pos > 0:
            ops.append('<')

        op = random.choice(ops)

        if op == '>':
            pos += 1
        elif op == '<':
            pos -= 1

        stub.append(op)

    invert = {
        '>': '<',
        '<': '>',
        '-': '+',
        '+': '-'}

    reverse = [invert[op] for op in reversed(stub)]

    return stub + reverse


if __name__ == '__main__':
    sys.exit(main())
