import argparse
import sys
import typing as t
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('file_path')

    args = parser.parse_args()

    run_file(Path(args.file_path))


def run_file(file_path: Path):
    content = file_path.read_text()
    run_code(content)


def run_code(code: str):
    memory = [0] * 256
    current_cell = 0
    brc = 0

    for i in range(len(code)):
        instruction = code[i]
        if instruction == '>':
            current_cell += 1
        elif instruction == '<':
            current_cell -= 1
        elif instruction == '+':
            memory[current_cell] += 1
        elif instruction == '-':
            memory[current_cell] -= 1
        elif instruction == '.':
            print(chr(memory[current_cell]), end='')
        elif instruction == ',':
            # TODO: Считывать символы или числа?
            memory[current_cell] = int(input())


if __name__ == '__main__':
    sys.exit(main())

