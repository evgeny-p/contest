import sys


def main():
    # 3 * swift = python
    for swift in range(10000, 100000):
        python = 3 * swift
        if is_valid_solution(swift, python):
            print("3 * {} = {}".format(swift, python))


def is_valid_solution(swift: int, python: int) -> bool:
    if 3 * swift != python:
        return False

    swift_str = str(swift)
    python_str = str(python)

    if swift_str[4] != python_str[2]:
        return False

    digits = set(swift_str) | set(python_str)

    return len(digits) == 10


if __name__ == '__main__':
    sys.exit(main())
