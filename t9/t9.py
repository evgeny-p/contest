# Не формулировал задание. Только демонстрация механики.
#
# На входе слово, на выходе последовательность нажатий на клавиши.
#
# 1 ij    2 abc   3 def
# 4 gh    5 kl    6 mn
# 7 prs   8 tuv   9 wxy
#         0 oqz
#
# Пример:
# > hello
# < 43550

import argparse
import sys


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('word')

    args = parser.parse_args()

    code = get_code(args.word)
    print(code)


def get_code(word: str) -> str:
    keys = [
        'oqz',
        'ij',  'abc', 'def',
        'gh',  'kl',  'mn',
        'prs', 'tuv', 'wxy']

    index = build_index(keys)
    code = ''.join(index[letter] for letter in word)

    return code


def build_index(keys):
    # Без обратного индекса алгоритм получается слишком сложным
    return {
        key: str(index)
        for index, seq in enumerate(keys)
        for key in seq}


if __name__ == '__main__':
    sys.exit(main())
