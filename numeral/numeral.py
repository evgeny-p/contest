import argparse
import sys
import typing as t
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('file_path')

    args = parser.parse_args()

    number = read_hex_number(Path(args.file_path))
    bits1 = count_bits1(number)
    bits2 = count_bits2(number)
    assert bits1 == bits2
    print(bits1)


def read_hex_number(file_path: Path) -> int:
    with open(str(file_path)) as input_file:
        line = input_file.readline()
        return int(line, base=16)


def count_bits1(number):
    counter = 0
    for digit in bin(number):
        if digit == '1':
            counter += 1
    return counter


def count_bits2(number):
    counter = 0
    for digit in bin(number)[2:]:
        counter += int(digit)
    return counter


if __name__ == '__main__':
    sys.exit(main())
